class User < ActiveRecord::Base
  has_and_belongs_to_many :webinars, :uniq => true
  has_secure_password
  
  attr_accessible :name, :email, :admin, :password
  
  validates :name,  presence: true, length: { maximum: 40 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 6 }
end


