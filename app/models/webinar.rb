class Webinar < ActiveRecord::Base
  attr_accessible :title, :location, :date, :time, :description

  validates(:title, :presence => true)
  validates(:location, :presence => true)
  validates(:date, :presence => true)
  validates(:time, :presence => true)
end
