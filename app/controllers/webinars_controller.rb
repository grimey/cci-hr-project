class WebinarsController < ApplicationController
  # GET /webinars
  # GET /webinars.json
  def index
    @webinars = Webinar.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @webinars }
    end
  end

  # GET /webinars/1
  # GET /webinars/1.json
  def show
    @webinar = Webinar.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @webinar }
    end
  end

  # GET /webinars/new
  # GET /webinars/new.json
  def new
    @webinar = Webinar.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @webinar }
    end
  end

  # GET /webinars/1/edit
  def edit
    @webinar = Webinar.find(params[:id])
  end

  # POST /webinars
  # POST /webinars.json
  def create
    @webinar = Webinar.new(params[:webinar])

    respond_to do |format|
      if @webinar.save
        format.html { redirect_to @webinar, notice: 'Webinar was successfully created.' }
        format.json { render json: @webinar, status: :created, location: @webinar }
      else
        format.html { render action: "new" }
        format.json { render json: @webinar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /webinars/1
  # PUT /webinars/1.json
  def update
    @webinar = Webinar.find(params[:id])

    respond_to do |format|
      if @webinar.update_attributes(params[:webinar])
        format.html { redirect_to @webinar, notice: 'Webinar was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @webinar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /webinars/1
  # DELETE /webinars/1.json
  def destroy
    @webinar = Webinar.find(params[:id])
    @webinar.destroy

    respond_to do |format|
      format.html { redirect_to webinars_url }
      format.json { head :no_content }
    end
  end
end
