class CreateWebinars < ActiveRecord::Migration
  def change
    create_table :webinars do |t|
      t.string :title
      t.string :location
      t.time :time
      t.date :date
      t.string :description

      t.timestamps
    end
  end
end
